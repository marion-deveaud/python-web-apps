---
title: Writing web applications using Python
revealOptions: 
    transition: 'fade'
---

### Writing web applications using Python

The purpose of this course is to give you an overview about how web applications are build and
let you program your first application using Python.

> All the materials used in the course are available online:
https://gitlab.com/marion-deveaud/python-web-apps.

<small>Everything is licensed under the [BSD 2-Clause "Simplified" license](https://choosealicense.com/licenses/bsd-2-clause/).</small>

----

## Part 1: my first web page

No programming experience in Python is required for this course!

But you need a working **programming environment**.

<image alt="Programming environment" src="dilbert-development-environment.jpeg"></image>

----

### 1. Install Python and configure a source code editor

* [Python](https://www.python.org/downloads/), use latest version if possible
* For Windows users:
  * You can use this [installer](https://www.python.org/ftp/python/3.7.2/python-3.7.2-amd64.exe)
  * While launching the installer:
    * **Choose the option "Add Python to PATH"**
  * Continue with the standard installation process

----

### 2. Install and configure Visual Code

* Install Visual Code from [this site](https://code.visualstudio.com/)
  * During the installation:
    * **Choose all options to make Code easily accessible**
  * Create a new directory for the training
    * E.g. `"moinworld-training"`
  * Start VC and open your project directory: File -> Open Folder
  * Use Icon "Extensions" on the left panel:
    * Install the `"Python"` extension
    * Optional: install [`"Pylint"`](https://code.visualstudio.com/docs/python/linting) (syntax checker)

----

### 3. Using Python package manager

* Verify that `"pip"` is installed by the standard Python package:
  * Start a terminal in Visual Code: Terminal -> New Terminal
  * From the terminal, type: `pip --help`
  * Or: `python -m pip --help` (under Windows the Python executable is called `py`)

* If `pip` is not available:
  * Download the installer file from [the pip webpage](https://bootstrap.pypa.io/get-pip.py)
  * Copy the file to the project directory
  * Start a terminal in Visual Code
  * Execute the file with Python: `python get-pip.py`

----

### **Optional**: correctly configuring Python on Windows

Normaly those variables should have been setup correctly using the Python installer but maybe you need to check it.

#### 1. Setting environment variables

* For VC, check [this documentation](https://code.visualstudio.com/docs/python/python-tutorial) to get help
* Make sure the path to your Python is setup correctly:
  * Search for environment variables in the system settings
  * Create a new variable `PYTHON_PATH` pointing to Python's installation directory
  * Update the variable `Path` to include `PYTHON_PATH`

----

### **Optional**: correctly configuring Python on Windows

#### 2. Verifying environment variables

* Verify the variables in VC or Command Prompt, or PowerShell:

```shell
{powershell}
> dir Env:PYTHON_PATH
> dir Env:Path
{Windows Command}
> echo %PYTHON_PATH%
> echo %PATH%
```

* If `python` is not in the PATH, you need to update your path
  * See previous slide

----

### 4. Technologies involved in web development

<image alt="Web application architecture" src="web-app-architecture.png"></image>

----

#### Furthermore

* Hosting & Continuous Deployment
* Data persistency
* Authentication and Security

----

### 5. Your first web project "profile yourself"

Main purpose of the application is to create a personal website with:

* A very personal resume
* A Kanban board to be used as to-do list
* And later on: create your own...
  * photo gallery
  * blog engine
  * interface for your home automation network
  * wedding planing page
  * cooking recipies

----

### 6. Install Python dependencies

> For this project we will use [Flask](http://flask.pocoo.org), a very simple web framework.

* Install `flask` using pip:

```shell
python -m pip install flask
```

* Download the web app project from [gitlab](https://gitlab.com/marion-deveaud/python-web-apps/-/archive/master/python-web-apps-master.zip)
* Unpack the ZIP file into a directory you will reuse during the course
* Or clone it using `git`:

```bash
git clone https://gitlab.com/marion-deveaud/python-web-apps.git
```

----

### 7. Starting the simple app

* Navigate into the directory `python-web-apps/flask`
* Edit the source file `app.py` and add your first page:

```python
@app.route("/")
def index():
    return "Welcome to my home page!"
```

* Run the flask application:

```shell
python app.py
```

* Call the site from your browser using: [http://127.0.0.1:5000](http://127.0.0.1:5000)
* Change the welcome message and reload the page

----

### 8. Using a HTML template for rendering

* Edit the source file `app.py` and use the flask method `render_template`:

```python
from flask import render_template
@app.route("/")
def index():
    return render_template('...', ...)
```

* Find the required template from the `templates` directory
* Use a dictionary to pass arguments to the template:

```python
my_page = {
  'title': 'Home',
  'text': 'Welcome to my home page!'
}
```

---

## Part 2: Create a profile page

#### 1. Use JSON to create your personal profile

* Fill out the prepared JSON file with your resume
  * Open [data/profile.json](data/profile.json)
  * Look at the data structure
  * Make some changes to add your own data

----

#### 2. Add a new route to your web application

* Edit `app.py` and create a new route:

```python
@app.route("/profile")
def me():
  ```

* Read the JSON file:

```python
with open('data/profile.json', 'r') as my_file:
  my_profile = json.load(my_file)
  print(json.dumps(my_profile, indent=4))
return "JSON file was found"
```

----

#### 3. Use a template to render information from the file

* Look for available templates in the directory `templates`:

```python
my_page = {'title': 'Profile'}
# TODO get data from JSON
return render_template('TODO', page=my_page, profile=my_profile)
```

* In the template, replace the TODO with data sections from the variable `profile`

---

### Part 3: Create a Kanban board

#### 1. Update application's dependencies

* For this part, you'll need the scripts from the Gitlab project:
  * `app_v2.py` from [flask/app_v2.py](https://gitlab.com/marion-deveaud/python-web-apps/tree/master/flask/app_v2.py)
  * `create_db.py` from [flask/create_db.py](https://gitlab.com/marion-deveaud/python-web-apps/tree/master/flask/create_db.py)

* Install new dependencies using:
  
```shell
pip install flask_sqlalchemy flask-wtf
or
py -m pip install flask_sqlalchemy flask-wtf
```

----

#### 2. Create a task list

* Have a look at the script `create_db.py` and answer those questions:
  * What will be the different statuses available in the Kanban application?
  * How many different tasks will be generated?

* Complete the TODO with the correct variables

* Edit `app_v2.py` and choose a name for the DB file (line 17)

* Save both scripts and run the initial setup of the DB:
  
```shell
python create_db.py
```

----

#### 3. Create routes to view, add and edit tasks

* Open the script `app_v2.py` and look for all TODOs
* Complete the script
* Run and debug the application using `python app_v2.py`

----

# Thank you for your attention and happy pythoning!

----

### Resources

* https://www.python.org/
* http://flask.pocoo.org/
* https://code.visualstudio.com/docs/python/python-tutorial
* https://aryaboudaie.com/python/technical/educational/web/flask/2018/10/17/flask.html
* https://www.w3schools.com/css/css_grid.asp
