# !/usr/bin/env python
#
# Copyright (C), Marion Deveaud 2018
#
# Licensed under the BSD-Clause 2, see LICENSE.md.

from app_v2 import db, Status, Task
from datetime import date

db.create_all()

doing = Status(name='Doing')
done = Status(name='Done')
planed = Status(name='Planed')
postponed = Status(name='Postponed')

for status in (TODO):
    print(f'Add status {status} to DB')
    db.session.add(status)
    db.session.commit()

task1 = Task(
    desc='Increase number of Women in Tech',
    status=Status.query.filter_by(name='Doing').first(),
    date=date(2018, 10, 27)
)

task2 = Task(
    desc='Invite Women to code',
    status=Status.query.filter_by(name='Done').first(),
    date=date(2018, 10, 12)
)

task3 = Task(
    desc='Create Python course',
    status=Status.query.filter_by(name='Doing').first(),
    date=date(2018, 11, 13)
)

task4 = Task(
    desc='Finish website',
    status=Status.query.filter_by(name='Planed').first(),
    date=date(2018, 12, 24)
)

for task in (TODO):
    print(f'Add task {task} to DB')
    db.session.add(task)
    db.session.commit()
