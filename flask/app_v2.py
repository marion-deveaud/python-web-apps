# !/usr/bin/env python
#
# Copyright (C), Marion Deveaud 2018
#
# Licensed under the BSD-Clause 2, see LICENSE.md.

import json

from datetime import datetime, date
from flask import Flask, render_template, flash, redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
from wtforms import Form, StringField, SelectField, DateField, FieldList, FormField, validators

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# Setup the name of your database file, e.g. kanban.db
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data/TODO'
db = SQLAlchemy(app)


class Status(db.Model):
    __tablename__ = 'status'
    id = db.Column(db.Integer, db.Sequence('status_id_seq'), primary_key=True)
    name = db.Column(db.String(15), unique=True)

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Status %r>' % (self.name)

class Task(db.Model):
    __tablename__ = 'task'
    id = db.Column(db.Integer, db.Sequence('kanban_id_seq'), primary_key=True)
    desc = db.Column(db.String(140), nullable=False, unique=True)
    status = db.Column(db.Integer, db.ForeignKey('status.id'))
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    status_id = db.Column(db.Integer, db.ForeignKey('status.id'), nullable=False)
    status = db.relationship('Status', backref=db.backref('kanbans', lazy=True))

    def __init__(self, desc=None, status=None, date=None):
        self.desc = desc
        self.status = status
        self.date = date

    def __repr__(self):
        return '<Task %r - Status %r - Due Date %r>' % (self.desc, self.status, self.date)

class TaskForm(Form):
    desc = StringField('Description', [validators.Length(min=4, max=300)])
    status = SelectField(
        'Task status',
        choices=[
            ('Doing', 'Doing'),
            ('Done', 'Done'),
            ('Planed', 'Planed'),
            ('Postponed', 'Postponed')
        ])
    date = DateField('Due date', format='%Y-%m-%d')

@app.route("/")
def index():
    my_page = {
        'title': 'Home',
        'text': 'Welcome to my home page!'
    }
    return render_template('home.html', page=my_page)


@app.route("/profile")
def me():
    my_page = {'title': 'Profile'}
    with open('data/profile.json', 'r') as my_file:
        my_profile = json.load(my_file)
        # print(json.dumps(my_profile, indent=4))
    return render_template('profile.html', page=my_page, profile=my_profile)


@app.route("/kanban")
def kanban():
    # Give a name to your Kanban board
    my_page = {'title': 'TODO'}
    
    tasks_per_status = []
    statuses = Status.query.all()
    for status in statuses:
        those_tasks = []
        tasks = Task.query.with_parent(status).all()
        for task in tasks:
            those_tasks.append({
                'desc': task.desc,
                'date': task.date.strftime("%Y-%m-%d"),
                'id': task.id
            })
        tasks_per_status.append({
            'status': status.name,
            'tasks': those_tasks
        })

    # Return the Kanban template
    # Set the required variables as arguments
    return render_template('TODO', page=TODO, tasks=TODO)


# Look-up in the template the path to the API endpoint for adding a new task 
@app.route("/kanban/TODO", methods=['GET', 'POST'])
def add_task():
    # Give a name to your task editor
    my_page = {'title': 'TODO'}
    my_form = TaskForm(request.form)
    
    if request.method == 'POST' and my_form.validate():
        status_id = Status.query.filter_by(name=my_form.status.data).first()
        if not Task.query.filter_by(desc=my_form.desc.data).first():
            task = Task(form.desc.data, status_id, form.date.data)
            # Add the new task in the DB
            db.session.add(TODO)
            db.session.commit()
        else:
            print('ERROR: task already exists')
        # Redirect the the main Kanban board once the new task as been created
        return redirect(url_for('TODO'))

    # Return the task addition/edition template
    # Set the required variables as arguments
    return render_template('TODO', form=TODO, page=TODO)


# Look-up in the template the path to the API endpoint for editing a task 
@app.route("/kanban/TODO/<int:task_id>", methods=['GET', 'POST'])
def edit_task(task_id):
    # Give a name to your task editor
    my_page = {'title': 'TODO'}
    my_form = TaskForm(request.form)
    # Get the task being edited using its id
    task = Task.query.get(TODO)

    if request.method == 'POST' and my_form.validate():
        status_id = Status.query.filter_by(name=my_form.status.data).first()
        task.desc = my_form.desc.data
        # Set the task status id
        task.status = TODO
        task.date = my_form.date.data
        db.session.commit()
        # Redirect to the main Kanban board once the task has been edited
        return redirect(url_for('TODO'))
    else:
        form.desc.data = task.desc
        form.status.data = task.status.name
        form.date.data = task.date
    
    # Return the task addition/edition template
    # Set the required variables as arguments
    return render_template('TODO', form=TODO, page=TODO)

if __name__ == '__main__':
	app.run(host='127.0.0.1')
