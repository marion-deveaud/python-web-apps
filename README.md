# Writing web applications using Python

Small code samples designed to teach young programmers how to code Python web apps.

Watch the presentation: https://marion-deveaud.gitlab.io/python-web-apps